Param(
	[string]$buildNumber
)

try
{
	Write-Host 'Starting...'
    $files = [xml](gc .\Containerchain.EDI.ProfileManager.WebUI\Containerchain.EDI.ProfileManager.WebUI.csproj);
    $versionSuffix = "dev+$buildNumber";
    $version = $files.Project.PropertyGroup[0].VersionPrefix + "-" + $versionSuffix;

    Remove-Item .\ContainerChain.EDI.ProfileManager.WebUI\published-app -Force -Recurse -ErrorAction:Ignore;
    Remove-Item .\ContainerChain.EDI.ProfileManager.WebUI\published-package -Force -Recurse -ErrorAction:Ignore;

    # Restore and build
	Write-Host 'Restoring'
    dotnet restore .\ContainerChain.EDI.ProfileManager.WebUI -s http://nuget.cchain.local:81/nuget/default -s https://api.nuget.org/v3/index.json
	
	Write-Host 'building'
    dotnet build .\ContainerChain.EDI.ProfileManager.WebUI\ContainerChain.EDI.ProfileManager.WebUI.csproj --configuration "Release" --version-suffix $versionSuffix ;
    
    # Publish
	Write-Host 'Publishing...'
    dotnet publish .\ContainerChain.EDI.ProfileManager.WebUI --output published-app --configuration "Release"

    # Create Octopus package
	Write-Host 'Creating Octopus package...'
    octo pack --basePath .\ContainerChain.EDI.ProfileManager.WebUI\published-app --id Containerchain.EDI.ProfileManager.ui --version $version --outFolder .\Containerchain.EDI.ProfileManager.WebUI\published-package;

	# Push to Octopus
	Write-Host 'Pushing to Octopus...'
    octo push --package=.\ContainerChain.EDI.ProfileManager.WebUI\published-package\Containerchain.EDI.ProfileManager.ui.$version.nupkg --server=http://build.containerchain.com.au:8888 --apiKey=API-HTBNSWLKMCIWQBZGQZQRN7U
    
	Write-Host "`nCreating the release..."
	octo create-release --project='EDI ProfileManager UI' --channel=Development --version=$version --server=http://build.containerchain.com.au:8888 --apiKey=API-HTBNSWLKMCIWQBZGQZQRN7U

}
catch
{
    Write-Host $_.Exception.Message
    exit 1
}