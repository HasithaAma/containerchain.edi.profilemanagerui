using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Containerchain.EDI.ProfileManager.Models;
using Microsoft.Extensions.Options;

namespace Containerchain_EDI_ProfileManager_WebUI.Controllers
{
    
    public class ConfigController : Controller
    {
        private readonly AppSettings _appSettings;

        public ConfigController(IOptions<AppSettings> appSettings)
        {
            _appSettings = appSettings.Value;
        }

        [Route("config")]
        [HttpGet]
        public IActionResult GetConfig()
        {
            return Ok(_appSettings);
        }

    }
}
