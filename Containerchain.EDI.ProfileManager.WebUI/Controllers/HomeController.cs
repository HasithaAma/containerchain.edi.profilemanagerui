using Microsoft.AspNetCore.Mvc;
using Containerchain.EDI.ProfileManager.Models;
using Microsoft.Extensions.Options;

namespace Containerchain_EDI_ProfileManager_WebUI.Controllers
{
    public class HomeController : Controller
    {

        public IActionResult Index()
        {
            return View();
        }

        public IActionResult Error()
        {
            return View();
        }
    }
}
