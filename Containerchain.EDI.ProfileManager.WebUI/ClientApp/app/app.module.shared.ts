import { NgModule, APP_INITIALIZER } from '@angular/core';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { BsModalService, ModalModule } from "ngx-bootstrap/modal";
import { ComponentLoaderFactory } from "ngx-bootstrap/component-loader/component-loader.factory";
import { PositioningService } from "ngx-bootstrap/positioning";
import { TabsModule, PopoverModule } from 'ngx-bootstrap';
import { AdalService } from 'ng2-adal/dist/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { GridModule } from '@progress/kendo-angular-grid';
import { ButtonsModule } from '@progress/kendo-angular-buttons';
import { DropDownsModule } from '@progress/kendo-angular-dropdowns';

// Components
import { AppComponent } from '../app/components/app/app.component';
import { NavMenuComponent } from '../app/components/navmenu/navmenu.component';
import { ProfileComponent } from '../app/components/profile/profile.component';
import { HeaderComponent } from '../app/components/header/header.component';
import { OutgoingSettingComponent } from '../app/components/filetransfer/outgoing/outgoingsetting.component';
import { ScheduleComponent } from '../app/components/schedule/schedule.component';
import { RouteGuard } from '../app/components/authentication/route.guard';
import { MessageBoxComponent } from '../app/components/shared/ui/messagebox.component';
import { ErrorMessageBoxComponent } from "./components/shared/ui/errormessagebox.component";
import { EDIFileComponent } from "./components/files/files.component";
import { ValidationControlComponent } from "./components/shared/ui/validationcontrol.component";
import { ValidationService } from "./components/shared/ui/validation.service";
import { Utilities } from "./components/shared/utils/utilities";
import { NewRecipientDialogComponent } from "./components/filetransfer/outgoing/newrecipientdialog.component";
import { IncomingSettingComponent } from './components/filetransfer/incoming/incomingsetting.component';
import { NewIncomingSettingDialogComponent } from './components/filetransfer/incoming/newincomingsettingdialog.component';
import { ConfirmBoxComponent } from './components/shared/ui/confirmbox.component';
import { FooterComponent } from './components/footer/footer.component';
import { HomeComponent } from './components/home/home.component';
import { OutputComponent } from "./components/output/output.component";
import { NewOutputDialogComponent } from "./components/output/newoutputdialog.component";
import { CronEditorModule } from "cron-editor/components/cron-editor/cron-editor.module";
import { NewOutgoingSettingDialogComponent } from "./components/filetransfer/outgoing/newoutgoingsettingdialog.component";
import { MappersComponent } from "./components/mappers/mappers.component";
import { SpinnerComponent } from "./components/shared/ui/spinner.component";

// Services
import { ProfileManagerService } from "../app/components/shared/services/profileManager.service";
import { GlobalService } from "../app/components/shared/services/global.service";
import { FileBankService } from "./components/shared/services/filebank.service";
import { MappersService } from "./components/shared/services/mappers.service";
import { AuthHttpService } from '../app/components/shared/services/authhttp.service';
import { SpinnerService } from "./components/shared/ui/spinner.service";
import { MapperHistoryComponent } from './components/mappers/mapperhistory.component';
import { ReportsService } from './components/shared/services/reports.service';
import { ReportsComponent } from "./components/reports/reports.component";
import { ReportHistoryComponent } from './components/reports/reporthistory.component';

export const sharedConfig: NgModule = {

    declarations: [
        AppComponent,
        NavMenuComponent,
        ProfileComponent,
        HeaderComponent,
        OutgoingSettingComponent,
        ScheduleComponent,
        MessageBoxComponent,
        ErrorMessageBoxComponent,
        EDIFileComponent,
        NewOutgoingSettingDialogComponent,
        OutputComponent,
        NewOutputDialogComponent,
        ValidationControlComponent,
        NewRecipientDialogComponent,
        IncomingSettingComponent,
        NewIncomingSettingDialogComponent,
        ConfirmBoxComponent,
        FooterComponent,
        HomeComponent,
        MappersComponent,
        SpinnerComponent,
        MapperHistoryComponent,
        ReportsComponent,
        ReportHistoryComponent
    ],
    imports: [
        RouterModule.forRoot([
            { path: '', component: AppComponent, canActivate: [RouteGuard], pathMatch: 'full' },
            {
                path: 'mappers', component: MappersComponent
            },
            {
                path: 'reports', component: ReportsComponent
            },
            { path: 'profiles/:name', component: HomeComponent,
                children: [
                    { path: '', component: ProfileComponent },
                    { path: 'outfiletransfer', component: OutgoingSettingComponent },
                    { path: 'infiletransfer', component: IncomingSettingComponent },
                    { path: 'schedule', component: ScheduleComponent },
                    { path: 'files', component: EDIFileComponent },
                    { path: 'output', component: OutputComponent }]
            },
            { path: '**', redirectTo: ''}
        ], { useHash: true }),
        FormsModule, ReactiveFormsModule, CronEditorModule, ModalModule.forRoot(), PopoverModule.forRoot(), TabsModule.forRoot()
        , BrowserModule, BrowserAnimationsModule, GridModule, ButtonsModule, DropDownsModule
    ],
    providers: [
        GlobalService, { provide: APP_INITIALIZER, useFactory: (global: GlobalService) => () => global.loadGlobals(), deps: [GlobalService], multi: true }//Before everything load global variables
        , RouteGuard
        , ProfileManagerService
        , FileBankService
        , AdalService
        , BsModalService
        , ComponentLoaderFactory
        , PositioningService
        , ValidationService
        , Utilities
        , AuthHttpService
        , MappersService
        , SpinnerService
        , ReportsService
    ],
    bootstrap: [AppComponent]
};
