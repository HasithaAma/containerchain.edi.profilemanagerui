﻿import { Component, ViewEncapsulation, OnInit, Input, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from "@angular/router";
import { MappersService } from "../shared/services/mappers.service";
import { GridDataResult, DataStateChangeEvent, FilterService } from '@progress/kendo-angular-grid';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import { State, CompositeFilterDescriptor, filterBy, FilterDescriptor } from '@progress/kendo-data-query';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { BsModalRef, BsModalService } from 'ngx-bootstrap';
import { MessageBoxComponent } from '../shared/ui/messagebox.component';
import { Subscription } from 'rxjs';
import { ProfileManagerService } from '../shared/services/profileManager.service';
const flatten = filter => {
    const filters = (filter || {}).filters;
    if (filters) {
        return filters.reduce((acc, curr) => acc.concat(curr.filters ? flatten(curr) : [curr]), []);
    }
    return [];
};

@Component({
    selector: 'mappers',
    templateUrl: './mappers.component.html',
    providers: [MappersService],
    styleUrls: [
        './mappers.component.scss'
    ],
    encapsulation: ViewEncapsulation.None
})

export class MappersComponent implements OnInit, OnDestroy {

    private fileToUpload: File = null;
    private mappers: any[];
    private remarks: string;
    private mapperHistoryDetails: any[];
    private mapperForm: FormGroup;
    private submitted: boolean;
    private mapperFile: string;
    private sub: any;
    private bsModalRef: BsModalRef;
    private subscriptions: Subscription[] = [];
    private profileType: string = "";
    private profileTypes: any[];
    public filter: CompositeFilterDescriptor;
    public gridData: any[] = filterBy(this.mappers, this.filter);

    constructor(private route: ActivatedRoute, private router: Router
        , private mapperService: MappersService
        , private modalService: BsModalService
        , private profileManagerService: ProfileManagerService) {
        this.createForm();
    }

    ngOnInit(): void {
        this.mapperService.getMappers(this.profileType).subscribe(mapperDetails => {
            this.gridData = this.mappers = mapperDetails;
        });

        this.profileManagerService.getProfileTypes().subscribe(profileTypes => {
            this.profileTypes = profileTypes;
        });
    }

    public categoryChange(values: any[], filterService: FilterService): void {
        filterService.filter({
            filters: values.map(value => ({
                field: 'profileType',
                operator: 'eq',
                value
            })),
            logic: 'or'
        });
    }

    public categoryFilters(filter: CompositeFilterDescriptor): FilterDescriptor[] {
        return flatten(filter).map(({ value }) => value);
    }

    public filterChange(filter: CompositeFilterDescriptor): void {
        this.filter = filter;
        this.gridData = filterBy(this.mappers, filter);
    }

    uploadMapperFile(isValid: boolean) {
        this.submitted = true;
        if (!isValid)
            return;

        this.mapperService.uploadMapperFile(this.fileToUpload, this.remarks, this.profileType).subscribe(data => {
            if (data) {
                this.bsModalRef = this.modalService.show(MessageBoxComponent);
                this.bsModalRef.content.title = 'Success';
                let message = "Mapper file:" + this.fileToUpload.name + " successfully saved.";
                this.bsModalRef.content.message = message;

                //Subscribe to events
                this.subscriptions.push(this.modalService.onHide.subscribe((reason: string) => {
                    this.mapperService.getMappers("").subscribe(mapperDetails => {
                        this.gridData = this.mappers = mapperDetails;
                    });
                }));
            }
        }, error => {
            console.log(error);
        });
    }

    handleFileInput(event) {
        this.fileToUpload = event.target.files[0];
        this.mapperForm.controls.mapperFile.setErrors(null);
    }

    createForm() {

        this.mapperForm = new FormGroup({
            mapperFile: new FormControl(this.mapperFile, Validators.required),
            remarks: new FormControl("", Validators.required),
            profileType: new FormControl("", Validators.required)
        });
    }

    isFieldValid(control: FormControl): boolean {
        return !(control.errors != null && (control.touched || control.dirty || this.submitted));
    }

    displayFieldCss(control: FormControl) {
        var valid = this.isFieldValid(control);
        return {
            'has-error': !valid,
            'has-feedback': !valid
        };
    }

    ngOnDestroy(): void {

    }
}