﻿import { Component, ViewEncapsulation, OnInit, Input } from '@angular/core';
import { ActivatedRoute, Router } from "@angular/router";
import { MappersService } from "../shared/services/mappers.service";
import { GridDataResult, DataStateChangeEvent } from '@progress/kendo-angular-grid';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import { State } from '@progress/kendo-data-query';
import { FormControl } from '@angular/forms';

@Component({
    selector: 'mapper-history',
    template: ` <kendo-grid
                [data] = "mapperhistory"
                [skip] = "skip"
                [scrollable] = "'none'"
                    (pageChange) = "pageChange($event)">
                    <kendo-grid-column field="eventTimestamp" title="Modified" width="250">
                    <ng-template kendoGridCellTemplate let-dataItem >
                        <span>{{dataItem.eventTimestamp | date:'yyyy-MM-dd HH:mm:ss a'}}</span>
                    </ng-template>
                    </kendo-grid-column>
                    <kendo-grid-column field="remarks" title="Remarks">
                    </kendo-grid-column>
                </kendo-grid>`,
    providers: [MappersService],
    styleUrls: [
        './mappers.component.scss'
    ],
    encapsulation: ViewEncapsulation.None
})

export class MapperHistoryComponent implements OnInit {

    @Input() private mapper: any;
    private  mapperhistory: any[];
    
    constructor(private route: ActivatedRoute, private router: Router, private mapperService: MappersService) {
    }

    ngOnInit(): void {

        this.mapperService.getMapperHistoryDetails(this.mapper.name).subscribe(hist=>this.mapperhistory = hist);
    }

   
}