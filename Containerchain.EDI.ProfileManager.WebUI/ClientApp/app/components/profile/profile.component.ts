import { Component, OnInit, OnDestroy } from '@angular/core';
import { EDIProfile, ProfileVm } from "../shared/data-types";
import { ActivatedRoute, Params, Router } from "@angular/router";
import { AppModule } from '../../app.module.client';
import { ProfileManagerService } from '../shared/services/profileManager.service';
import { MappersService } from '../shared/services/mappers.service';
import { MessageBoxComponent } from "../shared/ui/messagebox.component";
import { ErrorMessageBoxComponent } from "../shared/ui/errormessagebox.component";
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { BsModalRef, BsModalService } from "ngx-bootstrap/modal";
import { Subscription } from "rxjs/Subscription";
import { SpinnerService } from '../shared/ui/spinner.service';

@Component({
    selector: 'profile',
    templateUrl: './profile.component.html',
    providers: [ProfileManagerService]
})
export class ProfileComponent implements OnInit, OnDestroy {
    private profileName: string;
    private profileVm: ProfileVm;
    private sub: any;
    private profileForm: FormGroup;
    private bsModalRef: BsModalRef;
    private submitted: boolean = false;
    private subscriptions: Subscription[] = [];

    public loading: boolean;

    constructor(private route: ActivatedRoute, private profileManagerService: ProfileManagerService,
        private modalService: BsModalService, private router: Router, private mappersService: MappersService,
        private spinnerService: SpinnerService) {

        this.profileVm = new ProfileVm();
    }

    createForm(profile: EDIProfile) {
        this.profileForm = new FormGroup({
            profileName: new FormControl(profile.profileName, Validators.required),
            active: new FormControl(profile.active),
            ownerCommunityCode: new FormControl(profile.ownerCommunityCode, Validators.required),
            customerCommunityCode: new FormControl(profile.customerCommunityCode, Validators.required),
            profileType: new FormControl(profile.profileTypeId, Validators.required),
            mapperName: new FormControl(profile.mapperName, Validators.required)
        });
    }

    ngOnInit(): void {

        this.sub = this.route.parent.params.subscribe((param: any) => {

            this.profileName = param['name'];

            if (!this.profileName)
                return;

            if (this.profileName == '-1') {
                let profile = new EDIProfile();
                profile.profileId = -1;
                profile.active = true;
                this.profileVm.profile = profile;
                this.createForm(profile);
            }
            else {
                this.spinnerService.show('loadingSpinner');

                this.profileManagerService.getProfile(this.profileName).subscribe(pr => {
                    this.profileVm.profile = pr;
                    this.createForm(pr);

                    this.spinnerService.hide('loadingSpinner');
                });
            }

            this.profileManagerService.getProfileTypes().subscribe(pr => {
                this.profileVm.profileTypes = pr;
            });
        });

    }

    updateProfile(isValid) {

        this.submitted = true;

        if (!isValid)
            return;

        this.profileManagerService.updatePorfile(this.profileVm.profile).subscribe(pr => {
            //wait until save process completes
            if (pr) {
                this.bsModalRef = this.modalService.show(MessageBoxComponent);
                this.bsModalRef.content.title = 'Success';

                let message = "Profile:" + this.profileName + " successfully saved.";

                if (this.profileVm.profile.profileId == -1) {
                    message = "Successfully created the profile: " + this.profileVm.profile.profileName;
                }

                this.bsModalRef.content.message = message;

                //Subscribe to events
                this.subscriptions.push(this.modalService.onHide.subscribe((reason: string) => {

                    if (this.profileVm.profile.profileId == -1)
                        this.router.navigate(["/profiles"]);
                }));

            }
        }, (err) => {
            this.bsModalRef = this.modalService.show(ErrorMessageBoxComponent);
            this.bsModalRef.content.title = 'Error';
            this.bsModalRef.content.message = 'Error occured in saving profile: ' + this.profileName;

        });
    }

    ngOnDestroy(): void {
        this.sub.unsubscribe();
    }

    displayFieldCss(control: FormControl) {
        var valid = this.isFieldValid(control);
        return {
            'has-error': !valid,
            'has-feedback': !valid
        };
    }

    isFieldValid(control: FormControl): boolean {
        return !(control.errors != null && (control.touched || control.dirty || this.submitted));
    }

    public unsubscribe() {
        this.subscriptions.forEach((subscription: Subscription) => {
            subscription.unsubscribe();
        });
        this.subscriptions = [];
    }

    onProfileTypeChange(profileTypeId) {

        if (profileTypeId == null)
            return;

        var profileType = this.profileVm.profileTypes.find(pt => pt.profileTypeId ==profileTypeId);
        
        this.mappersService.getMappersByProfileType(profileType.name).subscribe(pts => {
            this.profileVm.mappers = pts.map(({ name }) => name);
        });
    }
}
