import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { AdalService } from "ng2-adal/dist/core";
import {GlobalService} from "../shared/services/global.service";

@Component({
    selector: 'app',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.css'],
    encapsulation: ViewEncapsulation.None
})
export class AppComponent implements OnInit {
    ngOnInit(): void {
        this.adalService.handleWindowCallback();

        this.adalService.getUser();
    }
    constructor(
        private adalService: AdalService,
        private globalService: GlobalService
    ) {

        var config = {
            tenant: globalService.AZUREAD_TENANT,
            clientId: globalService.AZUREAD_CLIENTID
        };

        this.adalService.init(config);

        
    }
}
