import { Component, Input, EventEmitter, Output } from '@angular/core';
import { AppModule } from '../../app.module.client';
import { ProfileVm, EDIProfile } from "../shared/data-types";
import { ActivatedRoute, Router } from '@angular/router';
import { ProfileManagerService } from "../shared/services/profileManager.service";
import { ConfirmBoxComponent } from '../shared/ui/confirmbox.component';
import { BsModalRef, BsModalService } from 'ngx-bootstrap';
import { MessageBoxComponent } from '../shared/ui/messagebox.component';
import { Subscription } from 'rxjs';

@Component({
    selector: 'footer',
    templateUrl: './footer.component.html'
})

export class FooterComponent {
    
    private bsModalRef: BsModalRef;
    private subscriptions: Subscription[] = [];
    @Input() profileName: string;
    private sub: any;

    constructor(private route: ActivatedRoute, private profileManagerService: ProfileManagerService, private modalService: BsModalService, private router: Router) {
    }

    deleteProfile() {

        this.bsModalRef = this.modalService.show(ConfirmBoxComponent);
        this.bsModalRef.content.message = "Deleting profile: " + this.profileName + ".";
        this.bsModalRef.content.confirmType = "profile";
        this.bsModalRef.content.confirmed.take(1).subscribe(this.deleteConfirmed.bind(this))
    }

    deleteConfirmed() {
        this.profileManagerService.deleteProfile(this.profileName).subscribe(pr => {
        },
            (err) => {
                this.bsModalRef = this.modalService.show(MessageBoxComponent);
                this.bsModalRef.content.title = 'Error';
                this.bsModalRef.content.message = "Error deleting profile: " + this.profileName + ".";
            },
            () => {
                this.bsModalRef = this.modalService.show(MessageBoxComponent);
                this.bsModalRef.content.title = 'Success';
                this.bsModalRef.content.message = "Profile: " + this.profileName + " successfully deleted.";

                //Subscribe to events
                this.subscriptions.push(this.modalService.onHide.subscribe((reason: string) => {
                    this.router.navigate(["/profiles"]);
                }));
            });
    }

    public unsubscribe() {
        this.subscriptions.forEach((subscription: Subscription) => {
            subscription.unsubscribe();
        });
        this.subscriptions = [];
    }
}
