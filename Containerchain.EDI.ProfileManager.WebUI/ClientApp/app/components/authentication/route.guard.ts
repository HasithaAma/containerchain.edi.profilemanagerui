﻿import { Injectable } from '@angular/core';
import { CanActivate } from '@angular/router';
import { AdalService } from 'ng2-adal/dist/core';

@Injectable()

export class RouteGuard implements CanActivate {
    constructor(private adalService: AdalService) { }
    canActivate() {
        if (this.adalService.userInfo.isAuthenticated) {
            return true;

        } else {
            this.adalService.login();
            return false;
        }
    }
}