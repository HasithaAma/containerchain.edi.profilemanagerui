﻿import { Component, OnInit } from "@angular/core";
import { ActivatedRoute } from "@angular/router";
import { ProfileManagerService } from "../shared/services/profileManager.service";
import { EDIProfile, Output } from "../shared/data-types";
import { BsModalRef, BsModalService } from "ngx-bootstrap/modal";
import { NewOutputDialogComponent } from "./newoutputdialog.component";
import { Subscription } from "rxjs/Subscription";
import { FormBuilder, FormGroup, Validators, FormArray, FormControl } from "@angular/forms";
import { MessageBoxComponent } from "../shared/ui/messagebox.component";
import { ErrorMessageBoxComponent } from "../shared/ui/errormessagebox.component";

@Component({
    selector: "output",
    templateUrl: "./output.component.html"
})

export class OutputComponent implements OnInit {
    private sub: any;
    private profile: EDIProfile;
    private contentTypes: string[];
    private bsModalRef: BsModalRef;
    private subscriptions: Subscription[] = [];
    private action: string = "";
    private newOutput: Output;
    private outputForm: FormGroup;
    private submitted: boolean;
    private profileName: string;

    constructor(private route: ActivatedRoute, private profileManagerService: ProfileManagerService, private modalService: BsModalService, private fb: FormBuilder) {
        this.profile = new EDIProfile();
        this.contentTypes = ["text/csv", "text/html", "application/json", "application/pdf", "application/rtf", "application/xml", "application/zip"];
    }

    ngOnInit(): void {
        this.sub = this.route.parent.params.subscribe((param: any) => {
            this.profileName = param["name"];

            if (!this.profileName || this.profileName == "-1")
                return;

            this.profileManagerService.getProfile(this.profileName).subscribe(pr => {
                this.profile = pr;
                //Create the reactive form
                this.createOutputForm(this.profile.outputs);
            });
        });
    }

    public addNewOutput() {

        this.newOutput = new Output();
        this.newOutput.name = "FILE" + (this.profile.outputs.length < 10 ? "0" + (this.profile.outputs.length+1) : this.profile.outputs.length+1);
        this.bsModalRef = this.modalService.show(NewOutputDialogComponent);
        this.bsModalRef.content.outputSetting = this.newOutput;
        this.bsModalRef.content.action = this.action;
        this.bsModalRef.content.lastFileId = this.profile.outputs.length;

        //Subscribe to events
        this.subscriptions.push(this.modalService.onHide.subscribe((reason: string) => {

            if (this.bsModalRef.content.action == "added") {

                //Add to the collection
                this.profile.outputs.push(this.newOutput);

                //Rebuild the form
                this.createOutputForm(this.profile.outputs);
            }
            this.unsubscribe();
        }));
    }

    removeOutput(id) {
        var indexOfItem = -1;

        indexOfItem = this.profile.outputs.findIndex(i => i.outputId == id);
        //Remove from the outputs collection
        this.profile.outputs.splice(indexOfItem, 1);

        //Rebuild the form
        this.createOutputForm(this.profile.outputs);
    }

    public unsubscribe() {
        this.subscriptions.forEach((subscription: Subscription) => {
            subscription.unsubscribe();
        });
        this.subscriptions = [];
    }

    updateProfile(isValid) {

        this.submitted = true;
        
        if (isValid == false)
            return;

        this.profileManagerService.updatePorfile(this.profile).subscribe(pr => {  //wait until save process completes
            if (pr) {
                this.bsModalRef = this.modalService.show(MessageBoxComponent);
                this.bsModalRef.content.title = 'Success';
                this.bsModalRef.content.message = 'Profile:' + this.profile.profileName + ' successfully saved.';
            }
        }, (err) => {
            this.bsModalRef = this.modalService.show(ErrorMessageBoxComponent);
            this.bsModalRef.content.title = 'Error';
            this.bsModalRef.content.message = 'Error occured in saving profile: ' + this.profile.profileName;
        });
    }

    createOutputForm(prOutputs: Output[]) {

        var formGroupArray = new Array();

        //Create a form group for each output section
        prOutputs.forEach(prOut => {
            formGroupArray.push(new FormGroup({
                name: new FormControl(prOut.name, Validators.required),
                fileNameTemplate: new FormControl(prOut.fileNameTemplate, Validators.required),
                contentType: new FormControl(prOut.contentType, Validators.required)
            }))
        });

        this.outputForm = this.fb.group({
            outputFormArray: this.fb.array(formGroupArray)
        });
    }

    displayFieldCss(control: FormControl) {
        var valid = this.isFieldValid(control);
        return {
            'has-error': !valid,
            'has-feedback': !valid
        };
    }

    isFieldValid(control: FormControl): boolean {
        return !(control.errors != null && (control.touched || control.dirty || this.submitted));
    }
}