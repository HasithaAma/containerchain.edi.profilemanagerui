import { Component, OnInit, OnDestroy} from '@angular/core';
import { ActivatedRoute } from "@angular/router";
import { ProfileVm, EDIProfile, Output } from "../shared/data-types";
import { BsModalRef } from 'ngx-bootstrap/modal';
import { FormGroup, FormBuilder, Validators, FormControl } from "@angular/forms";
import { ValidationControlComponent } from "../shared/ui/validationcontrol.component";
    
@Component({
    selector: 'newoutputdialog',
    templateUrl: './newoutputdialog.component.html'
})
export class NewOutputDialogComponent implements OnInit {
    
    private profileName: string;
    private profile: EDIProfile;
    private outputSetting: Output;
    private action: string;
    private newOutputForm: FormGroup;
    private submitted: boolean;
    private contentTypes: string[];
    
    constructor(public bsModalRef: BsModalRef, private fb: FormBuilder) {
        this.outputSetting = new Output();
        this.contentTypes = ["text/csv", "text/html", "application/json", "application/pdf", "application/rtf", "application/xml", "application/zip"];
    }
  
    ngOnInit(): void {
        //First show the ftp screen
        this.outputSetting.name = "FILE";
        this.createOutputForm(this.outputSetting);
        this.submitted = false;
    }

    createOutputForm(ut: Output) {
       
        this.newOutputForm = this.fb.group({
            name: [ut.name, Validators.required],
            fileNameTemplate: [ut.fileNameTemplate, Validators.required],
            contentType: [ut.contentType, Validators.required]
        });
    }

    addNewOutput(isValid) {
        
        this.submitted = true;
      
        if (!isValid)
            return;

        this.bsModalRef.hide();
        this.action = "added";
    }

    displayFieldCss(control: FormControl) {
        var valid = this.isFieldValid(control);
        return {
            'has-error': !valid,
            'has-feedback': !valid
        };
    }

    isFieldValid(control: FormControl): boolean {
        return !(control.errors != null && (control.touched || control.dirty || this.submitted));
    }
}
