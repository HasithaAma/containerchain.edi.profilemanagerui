import { Component, OnInit, OnDestroy, ViewChild, AfterViewInit } from '@angular/core';
import { ActivatedRoute } from "@angular/router";
import { ProfileManagerService } from "../../shared/services/profileManager.service";
import { ProfileVm, EDIProfile, EmailRecipient, IncomingEmailSetting, IncomingFtpSetting, DirectoryListenerSetting } from "../../shared/data-types";
import { NewIncomingSettingDialogComponent } from "./newincomingsettingdialog.component";
import { BsModalRef, BsModalService, ModalDirective } from "ngx-bootstrap/modal";
import { Subscription } from "rxjs/Subscription";
import { MessageBoxComponent } from "../../shared/ui/messagebox.component";
import { ErrorMessageBoxComponent } from "../../shared/ui/errormessagebox.component";
import { FormGroup, FormControl, Validators, FormBuilder, FormArray } from "@angular/forms";
import { Utilities } from "../../shared/utils/utilities";


@Component({
    selector: 'ftp',
    templateUrl: './incomingsetting.component.html',
    providers: [ProfileManagerService]
})
export class IncomingSettingComponent implements OnInit, OnDestroy {

    @ViewChild(ModalDirective) public modal: ModalDirective;
    private sub: any;
    private profileName: string;
    private profile: EDIProfile;
    private bsModalRef: BsModalRef;
    private subscriptions: Subscription[] = [];
    private messages: string[] = [];
    private newIncomingFtpSetting: IncomingFtpSetting;
    private newIncomingEmailSetting: IncomingEmailSetting;
    private newRecipient: EmailRecipient;
    private transferType: string;
    private action: string = "";
    private incomingSettingForm: FormGroup;
    private ftpItems: FormArray;
    private submitted: boolean;
    private newDirectoryListenerSetting: DirectoryListenerSetting;

    constructor(private route: ActivatedRoute, private profileManagerService: ProfileManagerService, private modalService: BsModalService, private fb: FormBuilder) {
        this.profile = new EDIProfile();
        this.transferType = "ftp";
    }

    ngOnInit(): void {
        this.route.parent.params.subscribe((param: any) => {
            this.profileName = param["name"];

            if (!this.profileName || this.profileName == "-1")
                return;

            this.profileManagerService.getProfile(this.profileName).subscribe(pr => {
                this.profile = pr;
                this.createFileTransferForm(this.profile);
            });
        });
    }

    public createNewIncomingSetting() {
        this.newIncomingFtpSetting = new IncomingFtpSetting();
        this.newIncomingEmailSetting = new IncomingEmailSetting();
        this.newDirectoryListenerSetting = new DirectoryListenerSetting();

        this.bsModalRef = this.modalService.show(NewIncomingSettingDialogComponent);
        this.bsModalRef.content.newIncomingFtpSetting = this.newIncomingFtpSetting;
        this.bsModalRef.content.newIncomingEmailSetting = this.newIncomingEmailSetting;
        this.bsModalRef.content.newDirectoryListenerSetting = this.newDirectoryListenerSetting;
        this.bsModalRef.content.transferType = this.transferType;
        this.bsModalRef.content.action = this.action;
        this.bsModalRef.content.hasSetting = (this.profile.outgoingEmailSetting != null || this.profile.incomingFtpSetting != null || this.profile.directoryListenerSetting != null);

        //Subscribe to events
        this.subscriptions.push(this.modalService.onHide.subscribe((reason: string) => {
            if (this.bsModalRef.content.action == "added") {

                if (this.bsModalRef.content.transferType == "ftp")
                    this.profile.incomingFtpSetting = this.newIncomingFtpSetting;
                else if (this.bsModalRef.content.transferType == "email")
                    this.profile.incomingEmailSetting = this.newIncomingEmailSetting;
                else
                    this.profile.directoryListenerSetting = this.newDirectoryListenerSetting;
            }
            this.unsubscribe();
            this.createFileTransferForm(this.profile);
        }));
    }

    updateProfile(isValid) {

        this.submitted = true;

        if (isValid == false)
            return;

        this.profileManagerService.updatePorfile(this.profile).subscribe(pr => {  //wait until save process completes
            if (pr) {
                this.bsModalRef = this.modalService.show(MessageBoxComponent);
                this.bsModalRef.content.title = 'Success';
                this.bsModalRef.content.message = 'Profile:' + this.profileName + ' successfully saved.';
            }
        }, (err) => {
            this.bsModalRef = this.modalService.show(ErrorMessageBoxComponent);
            this.bsModalRef.content.title = 'Error';
            this.bsModalRef.content.message = 'Error occured in saving profile: ' + this.profileName;
        });
    }

    ngOnDestroy(): void {
        return;
    }

    public unsubscribe() {
        this.subscriptions.forEach((subscription: Subscription) => {
            subscription.unsubscribe();
        });
        this.subscriptions = [];
    }

    removeIncomingSetting(type, id) {

        if (type == 'ftp')
            this.profile.incomingFtpSetting = null;
        else if (type == 'email')
            this.profile.incomingEmailSetting = null;
        else
            this.profile.directoryListenerSetting = null;

        this.createFileTransferForm(this.profile);
    }

    createFileTransferForm(profile: EDIProfile) {

        let ftpFormGroup = null;
        let emailFormGroup = null;
        let directoryListenerFormGroup = null;

        //Create a form group for each ftp section
        if (profile.incomingFtpSetting != null) {
            ftpFormGroup = new FormGroup({
                protocol: new FormControl(this.profile.incomingFtpSetting.protocol, Validators.required),
                serverName: new FormControl(this.profile.incomingFtpSetting.serverName, Validators.required),
                portNumber: new FormControl(this.profile.incomingFtpSetting.portNumber, Validators.required),
                username: new FormControl(this.profile.incomingFtpSetting.username, Validators.required),
                password: new FormControl(this.profile.incomingFtpSetting.password, Validators.required),
                directoryPath: new FormControl(this.profile.incomingFtpSetting.directoryPath, Validators.required)
            });

        }

        //Create a form group for each ftp section
        if (profile.incomingEmailSetting != null) {
            emailFormGroup = new FormGroup({
                recipientEmailAddress: new FormControl(profile.incomingEmailSetting.recipientEmailAddress, [Validators.required, Validators.email]),
                senderEmailAddress: new FormControl(profile.incomingEmailSetting.senderEmailAddress, [Validators.required, Validators.email]),
                subjectLine: new FormControl(profile.incomingEmailSetting.subjectLine, Validators.required),
                filename: new FormControl(profile.incomingEmailSetting.filename)
            });
        }

        //Create a form group for each ftp section
        if (profile.directoryListenerSetting != null) {
            directoryListenerFormGroup = new FormGroup({
                directoryPath: new FormControl(profile.directoryListenerSetting.directoryPath, Validators.required),
                filename: new FormControl(profile.directoryListenerSetting.filename)
            });
        }

        this.incomingSettingForm = this.fb.group({
            ftpFormGroup: ftpFormGroup,
            emailFormGroup: emailFormGroup,
            directoryListenerFormGroup: directoryListenerFormGroup
        });
    }

    displayFieldCss(control: FormControl) {

        var valid = this.isFieldValid(control);
        return {
            'has-error': !valid,
            'has-feedback': !valid
        };
    }

    isFieldValid(control: FormControl): boolean {
        return !(control.errors != null && (control.touched || control.dirty || this.submitted));
    }
}

