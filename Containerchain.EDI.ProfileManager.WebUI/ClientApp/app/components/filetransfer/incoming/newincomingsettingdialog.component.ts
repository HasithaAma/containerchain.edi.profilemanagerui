import { Component, OnInit, OnDestroy } from '@angular/core';
import { EDIProfile, IncomingEmailSetting, IncomingFtpSetting, DirectoryListenerSetting } from "../../shared/data-types";
import { BsModalRef } from 'ngx-bootstrap/modal';
import { FormGroup, FormBuilder, Validators, FormControl } from "@angular/forms";

@Component({
    selector: 'newfiletransferdialog',
    templateUrl: './newincomingsettingdialog.component.html'
})
export class NewIncomingSettingDialogComponent implements OnInit {

    public profileName: string;
    public profile: EDIProfile;
    public transferType: string;
    public newIncomingFtpSetting: IncomingFtpSetting;
    public newIncomingEmailSetting: IncomingEmailSetting;
    public newDirectoryListenerSetting: DirectoryListenerSetting;
    public action: string;
    public newIncomingSettingForm: FormGroup;
    public submitted: boolean;
    public hasSetting: boolean;

    constructor(public bsModalRef: BsModalRef, private fb: FormBuilder) {
        this.newIncomingFtpSetting = new IncomingFtpSetting();
        this.newIncomingEmailSetting = new IncomingEmailSetting();
        this.newDirectoryListenerSetting = new DirectoryListenerSetting();
        //Initial value sets up by the caller
        this.transferType = "";
    }

    ngOnInit(): void {
        //First show the ftp screen
        this.transferType = "ftp"; //default
        this.createNewFtpIncomingSetingForm(this.newIncomingFtpSetting);
        this.submitted = false;
    }

    createNewFtpIncomingSetingForm(ftp) {

        this.newIncomingSettingForm = this.fb.group({
            transferType: new FormControl(this.transferType, Validators.required),
            ftpFormGroup: this.fb.group({
                protocol: new FormControl(ftp.protocol, Validators.required),
                serverName: new FormControl(ftp.serverName, Validators.required),
                portNumber: new FormControl(ftp.portNumber),
                username: new FormControl(ftp.username, Validators.required),
                password: new FormControl(ftp.password, Validators.required),
                directoryPath: new FormControl(ftp.directoryPath, Validators.required)
            })
        });
    }

    createNewEmailIncomingSettingForm(email) {

        this.newIncomingSettingForm = this.fb.group({
            transferType: new FormControl(this.transferType, Validators.required),
            emailFormGroup: this.fb.group({
                recipientEmailAddress: new FormControl(email.recipientEmailAddress, [Validators.required, Validators.email]),
                senderEmailAddress: new FormControl(email.senderEmailAddress, [Validators.required, Validators.email]),
                subjectLine: new FormControl(email.subjectLine),
                filename: new FormControl(email.filename)
            })
        });
    }

    createNewDirectoryListenerIncomingSettingForm(dirList) {

        this.newIncomingSettingForm = this.fb.group({
            transferType: new FormControl(this.transferType, Validators.required),
            directoryListenerFormGroup: this.fb.group({
                directoryPath: new FormControl(dirList.directoryPath, Validators.required),
                filename: new FormControl(dirList.filename)
            })
        });
    }

    addNewFileTransfer(isValid) {

        this.submitted = true;

        if (!isValid)
            return;
        this.bsModalRef.hide();
        this.action = "added";
      
    }

    changeForm(transferType) {

        this.submitted = false;

        if (transferType == "ftp")
            this.createNewFtpIncomingSetingForm(this.newIncomingFtpSetting);
        else if (transferType == "email")
            this.createNewEmailIncomingSettingForm(this.newIncomingEmailSetting);
        else
            this.createNewDirectoryListenerIncomingSettingForm(this.newDirectoryListenerSetting);
    }


    displayFieldCss(control: FormControl) {
        var valid = this.isFieldValid(control);
        return {
            'has-error': !valid,
            'has-feedback': !valid
        };
    }

    isFieldValid(control: FormControl): boolean {
        return !(control.errors != null && (control.touched || control.dirty || this.submitted));
    }
}
