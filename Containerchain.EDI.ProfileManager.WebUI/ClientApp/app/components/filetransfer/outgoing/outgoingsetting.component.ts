import { Component, OnInit, OnDestroy, ViewChild, ViewEncapsulation } from '@angular/core';
import { ActivatedRoute } from "@angular/router";
import { ProfileManagerService } from "../../shared/services/profileManager.service";
import { ProfileVm, EDIProfile, EmailRecipient, OutgoingEmailSetting, OutgoingFtpSetting } from "../../shared/data-types";
import { NewOutgoingSettingDialogComponent } from "./newoutgoingsettingdialog.component";
import { BsModalRef, BsModalService, ModalDirective } from "ngx-bootstrap/modal";
import { Subscription } from "rxjs/Subscription";
import { MessageBoxComponent } from "../../shared/ui/messagebox.component";
import { ErrorMessageBoxComponent } from "../../shared/ui/errormessagebox.component";
import { FormGroup, FormControl, Validators, FormBuilder, FormArray } from "@angular/forms";
import { Utilities } from "../../shared/utils/utilities";
import { NewRecipientDialogComponent } from "./newrecipientdialog.component";

@Component({
    selector: 'outgoingsetting',
    templateUrl: './outgoingsetting.component.html',
    providers: [ProfileManagerService],
    encapsulation: ViewEncapsulation.None,
    styleUrls: ['../../../../../node_modules/@progress/kendo-theme-default/dist/all.css']
})
export class OutgoingSettingComponent implements OnInit, OnDestroy {

    @ViewChild(ModalDirective) public modal: ModalDirective;
    private sub: any;
    private profileName: string;
    private profile: EDIProfile;
    private bsModalRef: BsModalRef;
    private subscriptions: Subscription[] = [];
    private messages: string[] = [];
    private newOutgoingFtpSetting: OutgoingFtpSetting;
    private newOutgoingEmailSetting: OutgoingEmailSetting;
    private newRecipient: EmailRecipient;
    private transferType: string;
    private action: string = "";
    private outgoingSettingForm: FormGroup;
    private ftpItems: FormArray;
    private submitted: boolean;

    public ftpProtocols: Array<string> = ["FTP","SFTP"];

    constructor(private route: ActivatedRoute, private profileManagerService: ProfileManagerService, private modalService: BsModalService, private fb: FormBuilder) {
        this.profile = new EDIProfile();
        this.transferType = "ftp";
    }

    ngOnInit(): void {
        this.route.parent.params.subscribe((param: any) => {
            this.profileName = param["name"];

            if (!this.profileName || this.profileName == "-1")
                return;

            this.profileManagerService.getProfile(this.profileName).subscribe(pr => {
                this.profile = pr;
                this.createFileTransferForm(this.profile);
            });
        });
    }

    public createNewOutgoingSetting() {

        this.newOutgoingFtpSetting = new OutgoingFtpSetting();
        this.newOutgoingEmailSetting = new OutgoingEmailSetting();

        this.bsModalRef = this.modalService.show(NewOutgoingSettingDialogComponent);
        this.bsModalRef.content.newOutgoingFtpSetting = this.newOutgoingFtpSetting;
        this.bsModalRef.content.newOutgoingEmailSetting = this.newOutgoingEmailSetting;
        this.bsModalRef.content.transferType = this.transferType;
        this.bsModalRef.content.action = this.action;
        this.bsModalRef.content.hasEmailSetting = (this.profile.outgoingEmailSetting != null);

        //Subscribe to events
        this.subscriptions.push(this.modalService.onHide.subscribe((reason: string) => {
            if (this.bsModalRef.content.action == "added") {

                if (this.bsModalRef.content.transferType == "ftp")
                    Utilities.AddToList(this.profile.outgoingFtpSettings, this.newOutgoingFtpSetting);
                else
                    this.profile.outgoingEmailSetting = this.newOutgoingEmailSetting;
            }
            this.unsubscribe();
            this.createFileTransferForm(this.profile);
        }));
    }

    public addNewRecipient(settingId: number) {

        this.newRecipient = new EmailRecipient();
        this.bsModalRef = this.modalService.show(NewRecipientDialogComponent);
        this.bsModalRef.content.newRecipient = this.newRecipient;

        //Subscribe to events
        this.subscriptions.push(this.modalService.onHide.subscribe((reason: string) => {

            if (this.bsModalRef.content.action == "added")
                Utilities.AddToList(this.profile.outgoingEmailSetting.emailRecipients, this.newRecipient);

            this.unsubscribe();
            this.createFileTransferForm(this.profile);
        }));
    }

    updateProfile(isValid) {

        this.submitted = true;

        if (isValid == false)
            return;

        this.profileManagerService.updatePorfile(this.profile).subscribe(pr => {  //wait until save process completes
            if (pr) {
                this.bsModalRef = this.modalService.show(MessageBoxComponent);
                this.bsModalRef.content.title = 'Success';
                this.bsModalRef.content.message = 'Profile:' + this.profileName + ' successfully saved.';
            }
        }, (err) => {
            this.bsModalRef = this.modalService.show(ErrorMessageBoxComponent);
            this.bsModalRef.content.title = 'Error';
            this.bsModalRef.content.message = 'Error occured in saving profile: ' + this.profileName;
        });
    }

    ngOnDestroy(): void {
        return;
    }

    public unsubscribe() {
        this.subscriptions.forEach((subscription: Subscription) => {
            subscription.unsubscribe();
        });
        this.subscriptions = [];
    }

    removeOutgoingSetting(type, id) {
        var indexOfItem = -1;

        if (type == 'ftp') {
            indexOfItem = this.profile.outgoingFtpSettings.findIndex(i => i.outgoingFtpSettingId == id);
            this.profile.outgoingFtpSettings.splice(indexOfItem, 1);
        }
        else 
            this.profile.outgoingEmailSetting = null;

        this.createFileTransferForm(this.profile);
    }

    createFileTransferForm(profile: EDIProfile) {

        let ftpFormGroupArray = new Array();
        let emailFormGroup = null;

        //debugger
        //Create a form group for each ftp section
        profile.outgoingFtpSettings.forEach(ftp => {
            ftpFormGroupArray.push(new FormGroup({
                protocol: new FormControl(ftp.protocol, Validators.required),
                serverName: new FormControl(ftp.serverName, Validators.required),
                portNumber: new FormControl(ftp.portNumber, Validators.required),
                username: new FormControl(ftp.username, Validators.required),
                password: new FormControl(ftp.password, Validators.required),
                directoryPath: new FormControl(ftp.directoryPath, Validators.required)
            }));
        });

        //Create a form group for each ftp section
        let emailFg = null;

        if (profile.outgoingEmailSetting != null) {
            emailFormGroup = new FormGroup({
                fromAddress: new FormControl(profile.outgoingEmailSetting.fromAddress, [Validators.required, Validators.email]),
                fromDisplayName: new FormControl(profile.outgoingEmailSetting.fromDisplayName, Validators.required),
                subjectLine: new FormControl(profile.outgoingEmailSetting.subjectLine, Validators.required),
                emailBody: new FormControl(profile.outgoingEmailSetting.emailBody),
                isHtml: new FormControl(profile.outgoingEmailSetting.isHtml)
            });
        }

        this.outgoingSettingForm = this.fb.group({
            ftpFormGroupArray: this.fb.array(ftpFormGroupArray),
            emailFormGroup: emailFormGroup
        });
    }


    displayFieldCss(control: FormControl) {

        var valid = this.isFieldValid(control);
        return {
            'has-error': !valid,
            'has-feedback': !valid
        };
    }

    isFieldValid(control: FormControl): boolean {
        return !(control.errors != null && (control.touched || control.dirty || this.submitted));
    }

    removeRecipient(email: OutgoingEmailSetting, index: number) {
        email.emailRecipients.splice(index);
        this.createFileTransferForm(this.profile);
    }

    updateRecipient(email: OutgoingEmailSetting, recIndex: number) {
        
        this.bsModalRef = this.modalService.show(NewRecipientDialogComponent);
        this.bsModalRef.content.newRecipient = email.emailRecipients[recIndex];
        this.bsModalRef.content.action = "updated";

        //Subscribe to events
        this.subscriptions.push(this.modalService.onHide.subscribe((reason: string) => {
            this.unsubscribe();
            this.createFileTransferForm(this.profile);
        }));
    }
}

