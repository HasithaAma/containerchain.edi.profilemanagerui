import { Component, OnInit, OnDestroy } from '@angular/core';
import { ProfileVm, EDIProfile, EmailRecipient } from "../../shared/data-types";
import { BsModalRef } from 'ngx-bootstrap/modal';
import { FormGroup, FormBuilder, Validators, FormControl } from "@angular/forms";

@Component({
    selector: 'newrecipientdialog',
    templateUrl: './newrecipientdialog.component.html'
})
export class NewRecipientDialogComponent implements OnInit {

    public profileName: string;
    public profile: EDIProfile;
    public newRecipient: EmailRecipient;
    public action: string;
    public newRecipientForm: FormGroup;
    public submitted: boolean;

    constructor(public bsModalRef: BsModalRef, private fb: FormBuilder) {
        this.newRecipient = new EmailRecipient();
    }

    ngOnInit(): void {
        
        //First show the ftp screen
        this.createNewRecipientForm(this.newRecipient);
        this.submitted = false;
    }

    createNewRecipientForm(rec: EmailRecipient) {

        this.newRecipientForm = this.fb.group({
            displayName: [rec.displayName, Validators.required],
            emailAddress: [rec.emailAddress, [Validators.required, Validators.email]],
            cc: [rec.cc],
            bcc: [rec.bcc]
        });
    }

    displayFieldCss(control: FormControl) {
        var valid = this.isFieldValid(control);
        return {
            'has-error': !valid,
            'has-feedback': !valid
        };
    }

    isFieldValid(control: FormControl): boolean {
        return !(control.errors != null && (control.touched || control.dirty || this.submitted));
    }

    addNewRecipient(isValid) {

        this.submitted = true;

        if (!isValid)
            return;

        this.bsModalRef.hide();
        this.action = "added";
    }
}
