import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from "@angular/router";
import { ProfileVm, EDIProfile, OutgoingEmailSetting, OutgoingFtpSetting } from "../../shared/data-types";
import { BsModalRef } from 'ngx-bootstrap/modal';
import { FormGroup, FormBuilder, Validators, FormControl } from "@angular/forms";

@Component({
    selector: 'newfiletransferdialog',
    templateUrl: './newoutgoingsettingdialog.component.html'
})
export class NewOutgoingSettingDialogComponent implements OnInit {

    public profileName: string;
    public profile: EDIProfile;
    public transferType: string;
    public newOutgoingFtpSetting: OutgoingFtpSetting;
    public newOutgoingEmailSetting: OutgoingEmailSetting;
    public action: string;
    public newOutgoingSettingForm: FormGroup;
    public submitted: boolean;
    public hasEmailSetting: boolean;

    public ftpProtocols: Array<string> = ["FTP", "SFTP"];

    constructor(public bsModalRef: BsModalRef, private fb: FormBuilder) {
        this.newOutgoingFtpSetting = new OutgoingFtpSetting();
        this.newOutgoingEmailSetting = new OutgoingEmailSetting();
        //Initial value sets up by the caller
        this.transferType = "";
    }

    ngOnInit(): void {
        //First show the ftp screen
        this.transferType = "ftp"; //default
        this.createFtpForm(this.newOutgoingFtpSetting);
        this.submitted = false;
    }

    createFtpForm(ftp) {
        
        this.newOutgoingSettingForm = this.fb.group({
            transferType: new FormControl(this.transferType, Validators.required),
            ftpFormGroup: this.fb.group({
                protocol: new FormControl(ftp.protocol, Validators.required),
                serverName: new FormControl(ftp.serverName, Validators.required),
                portNumber: new FormControl(ftp.portNumber),
                username: new FormControl(ftp.username, Validators.required),
                password: new FormControl(ftp.password, Validators.required),
                directoryPath: new FormControl(ftp.directoryPath, Validators.required)
            })
        });

    }

    createEmailForm(email) {
        var emailFormGroupArray = new Array();
        
        this.newOutgoingSettingForm = this.fb.group({
            transferType: new FormControl(this.transferType, Validators.required),
            emailFormGroup: this.fb.group({
                fromAddress: new FormControl(email.fromAddress, [Validators.required, Validators.email]),
                fromDisplayName: new FormControl(email.fromDisplayName, Validators.required),
                subjectLine: new FormControl(email.subjectLine),
                emailBody: new FormControl(email.emailBody),
                isHtml: new FormControl(email.isHtml)
            })
        });
    }

    addnewFileTransfer(isValid) {
        
        this.submitted = true;

        if (!isValid)
            return;
        this.bsModalRef.hide();
        this.action = "added";
    }

    changeForm(transferType) {
        
        this.submitted = false;

        if (transferType == "ftp")
            this.createFtpForm(this.newOutgoingFtpSetting);
        else
            this.createEmailForm(this.newOutgoingEmailSetting);
    }


    displayFieldCss(control: FormControl) {
        var valid = this.isFieldValid(control);
        return {
            'has-error': !valid,
            'has-feedback': !valid
        };
    }

    isFieldValid(control: FormControl): boolean {
        return !(control.errors != null && (control.touched || control.dirty || this.submitted));
    }
}
