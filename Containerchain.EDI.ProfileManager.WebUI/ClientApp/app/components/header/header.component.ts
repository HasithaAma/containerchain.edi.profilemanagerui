import { Component, OnInit } from '@angular/core';
import { AppModule } from '../../app.module.client';
import { ProfileVm, EDIProfile } from "../shared/data-types";
import { ActivatedRoute, Router } from '@angular/router';
import { ProfileManagerService } from "../shared/services/profileManager.service";

@Component({
    selector: 'header',
    templateUrl: './header.component.html'
})

export class HeaderComponent implements OnInit {
    private profile: EDIProfile;
    private sub: any;
    private tabs: Tab[];
    private profileName: string;

    constructor(private route: ActivatedRoute, private profileManagerService: ProfileManagerService, private router: Router) {
        this.profile = new EDIProfile();
    }

    createTabs(profile: EDIProfile) {

        this.tabs = [
            { title: 'Main', name: null, active: false },
            { title: 'File Transfer', name: 'filetransfer', active: false },
            { title: 'Schedule', name: 'schedule', active: false },
            { title: 'Files', name: 'files', active: false },
            { title: 'Output', name: 'output', active: false }];

        //If create new profile checked, enable only first tab
        if (profile == null) {
            let mainTab = this.tabs.find(tb => tb.name == null);
            mainTab.active = true;
            this.tabs = [mainTab];
            return;
        }

        //Setup the file transfer direction route
        let directionPage = profile.profileTypeDirection == "Incoming" ? "infiletransfer" : "outfiletransfer";
        let filTransferTab = this.tabs.find(tb => tb.name == 'filetransfer');
        filTransferTab.name = directionPage;

        //Check for the main route
        if (this.router.url == "/profiles/" + profile.profileName) {
            var selTab = this.tabs.find(tb => tb.name == null);
            selTab.active = true;
            return;
        }

        //Extract current tab from url
        var index = this.router.url.lastIndexOf("/") + 1;
        var name = this.router.url.substring(index);

        //Make all tabs de-active
        this.tabs.forEach(tb => tb.active = false);

        //Set selected tab active
        var selTab = this.tabs.find(tb => tb.name == name);
        selTab.active = true;

    }

    //Ng route works for href only. Make it enable to the entire tab
    navigate(tab: string) {
        if (tab != null)
            this.router.navigateByUrl("/profiles/" + this.profile.profileName + "/" + tab);
        else
            this.router.navigateByUrl("/profiles/" + this.profile.profileName);
    }

    ngOnInit(): void {
        this.sub = this.route.params.subscribe((param: any) => {
            this.profileName = param['name'];
            
            if (this.profileName != null) {
                if (this.profileName == "-1") {
                    this.profile.profileName = "-1";
                    this.createTabs(null);
                }
                else {
                    this.profileManagerService.getProfile(this.profileName).subscribe(pr => {
                        this.profile = pr;
                        this.createTabs(this.profile);
                    });
                }

            }
        });
    }
}
export class Tab {
    title: string;
    name: string;
    active: boolean;
}