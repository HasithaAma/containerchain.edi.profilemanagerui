import { Component } from '@angular/core';
import { AppModule } from '../../app.module.client';
import { ProfileVm } from "../shared/data-types";
import { ActivatedRoute, Router } from '@angular/router';
import { AdalService } from "ng2-adal/dist/services";

@Component({
    selector: 'home',
    templateUrl: 'home.component.html'
})
export class HomeComponent  {
    public profileName: string;
    private sub: any;

    constructor(private route: ActivatedRoute, private adalService: AdalService, private router: Router) {
    }
}
