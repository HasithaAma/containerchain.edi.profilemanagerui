﻿import { Component, ViewEncapsulation, OnInit, Input } from '@angular/core';
import { ActivatedRoute, Router } from "@angular/router";
import { GridDataResult, DataStateChangeEvent } from '@progress/kendo-angular-grid';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import { State } from '@progress/kendo-data-query';
import { FormControl } from '@angular/forms';
import { ReportsService } from '../shared/services/reports.service';
import { DatePipe } from '@angular/common';

@Component({
    selector: 'report-history',
    template: ` <kendo-grid
                [data] = "reportHistory"
                [skip] = "skip"
                [scrollable] = "'none'"
                    (pageChange) = "pageChange($event)">
                    <kendo-grid-column field="eventTimestamp" title="Modified" width="250">
                        <ng-template kendoGridCellTemplate let-dataItem >
                            <span>{{dataItem.eventTimestamp | date:'yyyy-MM-dd HH:mm:ss a'}}</span>
                        </ng-template>
                    </kendo-grid-column>
                    <kendo-grid-column field="remarks" title="Remarks"></kendo-grid-column>
                </kendo-grid>`,
    providers: [ReportsService],
    encapsulation: ViewEncapsulation.None
})

export class ReportHistoryComponent implements OnInit {

    @Input() private report: any;
    private reportHistory: any[];

    constructor(private route: ActivatedRoute, private router: Router, private reportsService: ReportsService) {
    }

    ngOnInit(): void {

        this.reportsService.getReportHistoryDetails(this.report.name).subscribe(hist => this.reportHistory = hist);
    }

}