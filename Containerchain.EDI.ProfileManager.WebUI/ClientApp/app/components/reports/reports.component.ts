﻿import { Component, ViewEncapsulation, OnInit, Input, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from "@angular/router";
import { ReportsService } from "../shared/services/reports.service";
import { GridDataResult, DataStateChangeEvent } from '@progress/kendo-angular-grid';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import { State } from '@progress/kendo-data-query';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { BsModalRef, BsModalService } from 'ngx-bootstrap';
import { MessageBoxComponent } from '../shared/ui/messagebox.component';
import { Subscription } from 'rxjs';

@Component({
    selector: 'reports',
    templateUrl: './reports.component.html',
    providers: [ReportsService],
    encapsulation: ViewEncapsulation.None
})

export class ReportsComponent implements OnInit, OnDestroy {

    private fileToUpload: File = null;
    private reports: any[];
    private remarks: string = "";
    private sources: any = [];
    private reportHistoryDetails: any[];
    private reportForm: FormGroup;
    private submitted: boolean;
    private reportFile: string = "";
    private sub: any;
    private bsModalRef: BsModalRef;
    private subscriptions: Subscription[] = [];
    private sourceList: Array<string> = ["Repair estimate", "Gate movement", "Repair complete", "Storing order"];

    constructor(private route: ActivatedRoute, private router: Router
        , private reportsService: ReportsService
        , private modalService: BsModalService) {
        this.createForm();
    }

    ngOnInit(): void {
        this.reportsService.getReports().subscribe(reportDetails => {
            this.reports = reportDetails;
        });
    }

    uploadReportTemplate(isValid: boolean) {
        this.submitted = true;
        if (!isValid)
            return;

        this.reportsService.uploadReportTemplate(this.fileToUpload, this.sources, this.remarks).subscribe(data => {
            if (data) {
                this.bsModalRef = this.modalService.show(MessageBoxComponent);
                this.bsModalRef.content.title = 'Success';
                let message = "report file:" + this.fileToUpload.name + " successfully saved.";
                this.bsModalRef.content.message = message;
            }
        }, error => {
            console.log(error);
        });
    }

    handleFileInput(event) {
        this.fileToUpload = event.target.files[0];
        this.reportForm.controls.reportFile.setErrors(null);
    }

    createForm() {

        this.reportForm = new FormGroup({
            reportFile: new FormControl(this.reportFile, Validators.required),
            remarks: new FormControl(this.remarks, Validators.required),
            sources: new FormControl(this.sources, Validators.required)
        });
    }

    isFieldValid(control: FormControl): boolean {
        return !(control.errors != null && (control.touched || control.dirty || this.submitted));
    }

    displayFieldCss(control: FormControl) {
        var valid = this.isFieldValid(control);

        return {
            'has-error': !valid,
            'has-feedback': !valid
        };
    }

    ngOnDestroy(): void {

    }
}