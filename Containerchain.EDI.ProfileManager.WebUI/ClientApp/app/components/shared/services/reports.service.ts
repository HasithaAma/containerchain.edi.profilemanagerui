﻿import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions, Response } from "@angular/http";
import { EDIProfile, ProfileType } from "../data-types"
import { Observable } from "rxjs/Observable";
import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import { GlobalService } from './global.service';
import { AuthHttpService } from './authhttp.service';
import { guid } from '@progress/kendo-angular-grid/dist/es/utils';
@Injectable()

export class ReportsService {
    private reportingBaseUrl: string;

    constructor(private globalService: GlobalService, private reportingHttp: AuthHttpService) {
        
    }

    getReports(): Observable<string[]> {
      
        return this.reportingHttp.get(this.globalService.REPORT_API_URL + "reports")
            .map((res) => res.json())
            .catch((error: any) => Observable.throw(error.json().error || 'Server error'));
    }

    getReportHistoryDetails(templateName: string): Observable<any[]> {
        return this.reportingHttp.get(this.globalService.REPORT_API_URL + "reports/changehistory/" + templateName)
            .map((res) => res.json())
            .catch((error: any) => Observable.throw(error.json().error || 'Server error'));
    }

    uploadReportTemplate(file: File, sources:string, remarks: string): Observable<any> {
        
        let formData: FormData = new FormData();
        formData.append('template', file, file.name);
        formData.append('sources', sources);
        formData.append('remarks', remarks);
        
        return this.reportingHttp.postFileInput(this.globalService.REPORT_API_URL + "reports", formData, guid())
            .catch((error: any) => Observable.throw(error));
    }

}

