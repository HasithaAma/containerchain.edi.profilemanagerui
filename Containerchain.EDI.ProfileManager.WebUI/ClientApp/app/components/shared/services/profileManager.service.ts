﻿import { Injectable } from '@angular/core';
import { Http, Response } from "@angular/http";
import { EDIProfile, ProfileType } from "../data-types"
import { Observable } from "rxjs/Observable";
import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import { GlobalService } from './global.service';
import { AuthHttpService } from './authhttp.service';
@Injectable()

export class ProfileManagerService {
    private profManApiBaseUrl: any;

    constructor(private http: Http, private globalService: GlobalService, private profManagerHttp: AuthHttpService) {
        
    }

    getAllProfiles(): Observable<EDIProfile[]> {
        return this.profManagerHttp.get(this.globalService.PROFILEMANAGER_API_URL + "profiles")
            .map((res) => res.json())
            //...errors if any
            .catch((error: any) => Observable.throw(error));
    }

    getProfile(name: string): Observable<EDIProfile> {
        
        return this.profManagerHttp.get(this.globalService.PROFILEMANAGER_API_URL + 'profiles/' + name)
            .map((res) => res.json())
            .catch((error: any) => Observable.throw(error.json().error || 'Server error'));
    }

    updatePorfile(profile: EDIProfile): Observable<any> {

        let bodyString = JSON.stringify(profile); // Stringify payload

        return this.profManagerHttp.post(this.globalService.PROFILEMANAGER_API_URL + "profiles", profile).map(
            (res: Response) => { if (res) return true; }
            , (err) => { throw err; });
    }

    getProfileTypes(): Observable<ProfileType[]> {
        return this.profManagerHttp.get(this.globalService.PROFILEMANAGER_API_URL + "profiletypes")
            .map((res) => res.json())
            .catch((error: any) => Observable.throw(error.json().error || 'Server error'));
    }

    deleteProfile(profileName: string): Observable<any> {
        return this.profManagerHttp.delete(this.globalService.PROFILEMANAGER_API_URL + "profiles?profileName=" + profileName).map(
            (res: Response) => { if (res) return true; }
            , (err) => { throw err; });
    }
}

