﻿import { Injectable } from '@angular/core';

import { Observable } from "rxjs/Observable";
import { Http, Response, RequestOptions, Headers } from '@angular/http';
import { GlobalService } from './global.service';
import { AdalService } from 'ng2-adal/dist/core';


@Injectable()
export class AuthHttpService {

    constructor(private auth: AdalService, private http: Http, private globalService: GlobalService) { }

    public get(url: string): Observable<Response> {
        let options = new RequestOptions({ headers: this.getHeaders() });
        return this.http.get(url, options);
    }

    public post(url: string, payload: any) {
        let bodyString = JSON.stringify(payload); // Stringify payload
        let options = new RequestOptions({ headers: this.getHeaders() });
        return this.http.post(url, bodyString, options);
    }

    public postFileInput(url: string, payload: any, correlationId:any) {

        //For file inputs having content-type header causing issues
        let headers: Headers = new Headers();
        headers.append('correlationId',correlationId);
        headers.append('Accept', 'application/json');
        headers.append('Authorization', `Bearer ${this.getToken()}`);

        let options = new RequestOptions({ headers: headers });
        
        return this.http.post(url, payload, options);
    }

    public delete(url: string) {
        let options = new RequestOptions({ headers: this.getHeaders() });
        return this.http.delete(url, options);
    }

    getHeaders(): Headers {
        let headers: Headers = new Headers();

        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', `Bearer ${this.getToken()}`);

        return headers;
    }

    getToken(): string {
        let token: string;
        this.auth.acquireToken(this.globalService.AZUREAD_CLIENTID).subscribe(x => token = x);

        if (!token)
            this.auth.login();

        return token;
    }
}