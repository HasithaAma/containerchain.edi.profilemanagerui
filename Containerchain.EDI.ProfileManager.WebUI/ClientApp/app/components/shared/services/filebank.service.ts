﻿import { Injectable } from '@angular/core';
import { Http } from "@angular/http";
import { FileBankFile } from "../data-types"
import { Observable } from "rxjs/Observable";
import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import { GlobalService } from './global.service';
import { AuthHttpService } from './authhttp.service';
import { GridDataResult } from '@progress/kendo-angular-grid';
import { EdiFileList } from "../models/files.models";
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { SpinnerService } from '../ui/spinner.service';

@Injectable()

export class FileBankService extends BehaviorSubject<GridDataResult>{

    constructor(private http: Http, private globalService: GlobalService, private fileBankHttp: AuthHttpService, private spinnerService: SpinnerService) {
        super(BehaviorSubject.create());
    }

    public query(profileName: string, state: any, spinnerName: string): void {
        this.getEDIFiles(profileName, state)
            .subscribe(x => {
                this.spinnerService.hide('loadingSpinner');
                super.next(x);
            });
    }

    private getEDIFiles(profileName: string, state: any): Observable<GridDataResult> {
        return this.fileBankHttp.get(this.globalService.FILEBANK_API_URL + "files/getbyprofile/" + profileName + `?pageSize=${state.take}&skip=${state.skip}`)
            .map(response => (<GridDataResult>{
                data: (response.json() as EdiFileList).list,
                total: (response.json() as EdiFileList).totalCount
            }))
            .catch((error: any) => Observable.throw(error));
    }

    public downloadEdiFile(fileGuid: string): Observable<any> {

        return this.fileBankHttp.get(this.globalService.FILEBANK_API_URL + "files/" + fileGuid)
            .catch((error: any) => Observable.throw(error));
    }

    public reprocessEdiFile(fileGuid: string): Observable<any> {
        return this.fileBankHttp.post(this.globalService.FILEBANK_API_URL + "files/reprocess/" + fileGuid, "")
            .catch((error: any) => Observable.throw(error));
    }
}

