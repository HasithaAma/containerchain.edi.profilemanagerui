﻿import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions, Response } from "@angular/http";
import { Observable } from "rxjs/Observable";
import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import { GlobalService } from './global.service';
@Injectable()

export class MappersService {
    constructor(private http: Http, private globalService: GlobalService) {

    }

    getMappers(profileType: string): Observable<any[]> {
        return this.http.get(this.globalService.MAPPER_API_URL + "mappers?profileType=" + profileType)
            .map((res) => res.json())
            .catch((error: any) => Observable.throw(error.json().error || 'Server error'));
    }

    getMapperHistoryDetails(mapperName: string): Observable<any[]> {
        return this.http.get(this.globalService.MAPPER_API_URL + "mapperhistory/" + mapperName)
            .map((res) => res.json())
            .catch((error: any) => Observable.throw(error.json().error || 'Server error'));
    }

    uploadMapperFile(file: File, remarks: string, profileType: string): Observable<any>{
        
        let formData: FormData = new FormData();
        formData.append('mapperFile', file, file.name);
        formData.append('remarks', remarks);
        formData.append('profileType', profileType);
        let headers = new Headers();
        /** No need to include Content-Type in Angular 4 */
        //headers.append('Content-Type', 'multipart/form-data');
        headers.append('Accept', 'application/json');
        let options = new RequestOptions({ headers: headers });

        return this.http.post(this.globalService.MAPPER_API_URL + "mappers", formData, options)
            .map((res: Response) => { if (res) return true; }
            , (err) => { throw err; });
    }

    getMappersByProfileType(profileType: string): Observable<any[]> {
        return this.http.get(this.globalService.MAPPER_API_URL + "mappers?profileType=" + profileType)
            .map((res) => res.json())
            .catch((error: any) => Observable.throw(error.json().error || 'Server error'));
    }
}