﻿import { Injectable } from '@angular/core';
import { Http } from "@angular/http";
import 'rxjs/add/operator/toPromise';

@Injectable()

export class GlobalService {

    public PROFILEMANAGER_API_URL: string;
    public MAPPER_API_URL: string;
    public FILEBANK_API_URL: string;
    public REPORT_API_URL: string;

    public AZUREAD_CLIENTID: string;
    public AZUREAD_TENANT: string;

    constructor(private http: Http) {
    }

    loadGlobals() {

        return this.getApiUrls().then((res: any) => {
            this.PROFILEMANAGER_API_URL = res.apiUrls.profileManagerApiBaseUrl;
            this.MAPPER_API_URL = res.apiUrls.mapperApiBaseUrl;
            this.FILEBANK_API_URL = res.apiUrls.fileBankApiBaseUrl;
            this.REPORT_API_URL = res.apiUrls.reportApiBaseUrl;
            
            this.AZUREAD_CLIENTID = res.authentication.clientId;
            this.AZUREAD_TENANT = res.authentication.tenant;
        });
    };

    getApiUrls(): Promise<any> {

        return this.http.get('config')
            .toPromise()
            .then((res) => res.json())
            .catch((error: any) => { throw error });
    }
};

