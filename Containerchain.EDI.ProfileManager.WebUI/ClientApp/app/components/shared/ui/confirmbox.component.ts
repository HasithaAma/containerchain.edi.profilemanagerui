import { Component, EventEmitter } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap/modal';

@Component({
    selector: 'confirmbox',
    templateUrl: './confirmbox.component.html'
})
export class ConfirmBoxComponent {
    public confirmed: EventEmitter<any> = new EventEmitter();

    constructor(public bsModalRef: BsModalRef) {
       
    }
    confirm() {
        this.confirmed.emit(true);
        this.bsModalRef.hide();
    }
}