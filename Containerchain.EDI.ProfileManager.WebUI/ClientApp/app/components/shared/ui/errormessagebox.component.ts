import { Component } from '@angular/core';
import { BsModalRef } from "ngx-bootstrap/modal";

@Component({
    selector: 'errormessagebox',
    templateUrl: './errormessagebox.component.html'
})
export class ErrorMessageBoxComponent {

    constructor(public bsModalRef: BsModalRef) {
       
    }
}