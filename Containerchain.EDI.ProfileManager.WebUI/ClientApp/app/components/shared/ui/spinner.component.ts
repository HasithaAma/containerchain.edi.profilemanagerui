﻿import { Component, Input, OnInit, OnDestroy } from '@angular/core';
import { SpinnerService } from './spinner.service';

@Component({
    selector: 'spinner',
    template: `
<div *ngIf="show" class="loading-overlay">
    <span class="centered fa fa-circle-o-notch fa-spin fa-3x fa-fw"></span>
</div>
  `
})
export class SpinnerComponent implements OnInit, OnDestroy {
    
    constructor(private spinnerService: SpinnerService) { }

    ngOnInit(): void {
        this.spinnerService.register(this);
    }

    ngOnDestroy(): void {
        this.spinnerService.unregister(this);
    }

    @Input() show = false;
    @Input() name: string;
}  