import { Component, Input } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { ValidationService } from './validation.service';

@Component({
    selector: 'val-message',
    template: `<label class='control-label label-error pull-right' *ngIf="errorMessage !== null">{{errorMessage}}</label>`
})
export class ValidationControlComponent {
    @Input() control: FormControl;
    @Input() submitted: boolean = false;

    constructor() {

    }

    get errorMessage() {
      
        for (let propertyName in this.control.errors) {
            if (this.control.errors.hasOwnProperty(propertyName) && !this.isFieldValid(this.control)) {
                return ValidationService.getValidatorErrorMessage(propertyName, this.control.errors[propertyName]);
            }
        }

        return null;
    }

    private isFieldValid(control: FormControl): boolean {
        return !(control.errors != null && (control.touched || control.dirty || this.submitted));
    }
}

