﻿export class EDIProfile {
    profileId: number;
    profileName: string = '';
    active: boolean 
    profileTypeId: number;
    profileOwnerId: number;
    mapperName: string ='';
    ownerCommunityCode: string = '';
    customerCommunityCode: string = '';
    outgoingFtpSettings: OutgoingFtpSetting[];
    outgoingEmailSetting: OutgoingEmailSetting;
    incomingFtpSetting: IncomingFtpSetting;
    incomingEmailSetting: IncomingEmailSetting;
    cronExpression: string = '';
    outputs: Output[];
    profileTypeDirection: string = '';
    directoryListenerSetting: DirectoryListenerSetting;
};

export class ProfileVm {
    profile :EDIProfile;
    mappers: string[];
    movementTypes: string[];
    profileTypes: ProfileType[];
};

export class ProfileType {
    profileTypeId: number;
    name: string;
};

export class FileBankFile{
    fileID: string;
    fileStorageKey: string;
    ownerCommunityCode: string;
    customerCommunityCode: string;
    profileName: string;
    fileType: string;
    contentType: string;
    filename: string;
    created: string;
    updated: string;
    history: EDIFileHistory[]
};

export class Output{
    outputId: number;
    profileId: number;
    name: string = '';
    fileNameTemplate: string = '';
    contentType: string =''; 
}

export class EDIFileHistory {
    eventName: string;
    timeStamp: string;
    remarks: string;
}

export class EDIResult {
    status: string;
    message: string;
}

export class EmailRecipient {
    emailRecipientId: number;
    displayName: string = "";
    emailAddress: string = "";
    cc: string = "";
    bcc: string = "";
}

export class OutgoingFtpSetting {
    outgoingFtpSettingId: number;
    protocol: string = '';
    serverName: string = '';
    portNumber: number;
    username: string = '';
    password: string = '';
    directoryPath: string = '';
}

export class OutgoingEmailSetting {
    outgoingEmailSettingId: number;
    fromAddress: string = '';
    fromDisplayName: string = '';
    subjectLine: string = "";
    emailBody: string = '';
    isHtml: boolean = false;
    emailRecipients: EmailRecipient[] = [];
};

export enum ProfileTypeDirection{
    Incoming,
    Outgoing
}

export class IncomingFtpSetting {
    outgoingFtpSettingId: number;
    protocol: string = '';
    serverName: string = '';
    portNumber: number;
    username: string = '';
    password: string = '';
    directoryPath: string = '';
}

export class IncomingEmailSetting {
    incomingEmailSettingId: number;
    recipientEmailAddress: string = '';
    senderEmailAddress: string = '';
    subjectLine: number;
    filename: string = '';
}

export class DirectoryListenerSetting {
    directoryListenerSettingId: number;
    directoryPath: string = '';
    filename: string = '';
}