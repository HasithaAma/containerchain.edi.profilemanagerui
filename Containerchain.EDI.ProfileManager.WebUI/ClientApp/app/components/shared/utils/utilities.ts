interface Array<T> {
    contain(item: T): boolean;
}  

export class Utilities {

    public static AddToList(source: any[], item: any) {
        let newItem = {};

        for (var property in item) {
            if (item.hasOwnProperty(property)) {
                newItem[property] = item[property];
            }
        }
        source.push(newItem);
    }
}