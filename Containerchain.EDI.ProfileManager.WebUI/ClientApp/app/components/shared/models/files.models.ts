﻿export class EdiFileList {
    totalCount: number;
    list: EdiFile[];
}

export class EdiFile {
    fileStorageKey: string;
    ownerCommunityCode: string;
    fileType: FileType;
    contentType: string;
    filename: string;
    history: FileHistory[];
}

export class FileType {
    code: string;
    description: string;
}

export class FileHistory {
    fileEvent: FileEvent;
    remarks: string;
    timestamp: Date;
}

export class FileEvent {
    code: string;
    description: string;
}