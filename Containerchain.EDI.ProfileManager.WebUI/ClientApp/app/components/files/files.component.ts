import { Component, OnInit, OnDestroy, ViewEncapsulation } from '@angular/core';
import { ActivatedRoute } from "@angular/router";
import { FileBankService } from "../shared/services/filebank.service";
import { GlobalService } from "../shared/services/global.service";
import { BsModalService, BsModalRef } from "ngx-bootstrap/modal";
import { GridDataResult, DataStateChangeEvent } from '@progress/kendo-angular-grid';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import { State } from '@progress/kendo-data-query';
import { SpinnerService } from '../shared/ui/spinner.service';

@Component({
    selector: 'files',
    templateUrl: './files.component.html',
    providers: [FileBankService],
    styleUrls: [
        './files.component.scss'
    ],
    encapsulation: ViewEncapsulation.None
})
export class EDIFileComponent implements OnInit, OnDestroy  {
    private sub: any;
    public profileName: string;
    public downloadUrl: string;

    public view: Observable<GridDataResult>;
    public state: State = {
        skip: 0,
        take: 20
    };

    constructor(private route: ActivatedRoute, private fileBankService: FileBankService,
        private globalService: GlobalService, private modalService: BsModalService,
        private spinnerService: SpinnerService) {
        this.downloadUrl = this.globalService.FILEBANK_API_URL + 'files\\';
  
    }

    ngOnInit(): void {

        this.sub = this.route.parent.params.subscribe((param: any) => {
            this.profileName = param["name"];

            if (!this.profileName || this.profileName == "-1")
                return;
        });

        this.view = this.fileBankService;
        this.spinnerService.show('loadingSpinner');
        this.fileBankService.query(this.profileName, this.state, 'loadingSpinner');
    }

    public dataStateChange(state: DataStateChangeEvent): void {
        this.state = state;
        this.spinnerService.show('loadingSpinner');
        this.fileBankService.query(this.profileName, state, 'loadingSpinner');
    }

    ngOnDestroy(): void {
        this.sub.unsubscribe();
    }

    public reprocessFile(fileId: string) {
        this.spinnerService.show('loadingSpinner');
        this.fileBankService.reprocessEdiFile(fileId).subscribe(x => this.spinnerService.hide('loadingSpinner'));
    }

}
