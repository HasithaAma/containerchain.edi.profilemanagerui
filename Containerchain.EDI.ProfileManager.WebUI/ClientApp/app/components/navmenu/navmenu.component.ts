import { Component, OnInit, OnDestroy, ViewEncapsulation } from '@angular/core';
import { EDIProfile } from "../shared/data-types";
import { ProfileManagerService } from "../shared/services/profileManager.service";
import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
@Component({
    selector: 'nav-menu',
    templateUrl: './navmenu.component.html',
    styleUrls: ['./navmenu.component.css', '../../../../node_modules/@progress/kendo-theme-default/dist/all.css'],
    encapsulation: ViewEncapsulation.None
})
export class NavMenuComponent implements OnInit, OnDestroy  {
    ngOnInit(): void {
        if (!this.profiles) {
            this.sub = this.profileManagerService.getAllProfiles().subscribe(pr => {
                this.profiles = pr;
                this.filteredProfiles = pr;
            }); 
        }
    }
    menuIconClass: string = 'k-icon k-i-cog';

    data: Array<any> = [{
        text: 'Mappers',
        link: '/mappers'
    }, {
        text: 'Reports',
        link: '/reports'
    }];

    public profiles: EDIProfile[];
    public filteredProfiles: any;
    private profileName: string;
    private sub: any;

    constructor(private profileManagerService: ProfileManagerService) {
        
    }

    filterProfiles(searchTerm: string) {
        if (searchTerm == null || searchTerm == "" || searchTerm.length < 2)
            this.filteredProfiles = this.profiles;

        searchTerm = searchTerm.toUpperCase();

        this.filteredProfiles = this.profiles.filter(pr => pr.profileName.toLocaleUpperCase().search(searchTerm) > -1);
    }

    ngOnDestroy() {
        this.sub.unsubscribe();
    }

}
