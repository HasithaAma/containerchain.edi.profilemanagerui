import { Component, OnInit, OnDestroy, ElementRef, AfterViewInit } from '@angular/core';
import { EDIProfile } from "../shared/data-types";
import { ProfileManagerService } from "../shared/services/profileManager.service";
import { Router, ActivatedRoute, NavigationEnd } from "@angular/router";
import { sharedConfig } from '../../app.module.shared';
import { forEach } from "@angular/router/src/utils/collection";
import { CronEditorModule, CronOptions } from "cron-editor/cron-editor";
import { ErrorMessageBoxComponent } from "../shared/ui/errormessagebox.component";
import { MessageBoxComponent } from "../shared/ui/messagebox.component";
import { BsModalRef, BsModalService } from "ngx-bootstrap/modal";

export class SelectList {
    Name: string;
    Id: string;
    Selected: boolean;
};

@Component({
    selector: 'schedule',
    templateUrl: './schedule.component.html',
    providers: [ProfileManagerService]

})
export class ScheduleComponent implements OnInit {
    public cronTabExp: string;
    public sub: any;
    public profile: EDIProfile;
    public cronExpression = '';
    public isCronDisabled = false;
    public bsModalRef: BsModalRef;
    private profileName: string;
    private defaultCronExpression = '0 0 1 1 1/1 *';

    public cronOptions: CronOptions = {
        formInputClass: 'form-control cron-editor-input',
        formSelectClass: 'form-control cron-editor-select',
        formRadioClass: 'cron-editor-radio',
        formCheckboxClass: 'cron-editor-checkbox',
        defaultTime: "10:00:00",
        hideMinutesTab: false,
        hideHourlyTab: false,
        hideDailyTab: false,
        hideWeeklyTab: false,
        hideMonthlyTab: false,
        hideYearlyTab: true,
        hideAdvancedTab: false,
        use24HourTime: true,
        hideSeconds: true
    };

    constructor(private profileManagerService: ProfileManagerService, private router: Router, private route: ActivatedRoute, private modalService: BsModalService, private cronEditor: ElementRef) {
    }

    ngOnInit(): void {

        //Populate profile details
        this.sub = this.route.parent.params.subscribe((param: any) => {

            this.profileName = param['name'];

            if (!this.profileName || this.profileName == "-1")
                return;

            this.profileManagerService.getProfile(this.profileName).subscribe(pr => {
                this.profile = pr;
                //Hangfire supports only 5 components in the cron that needs to be saved in the database. So need to convert it to the regular that supports the schedule component.
                if (pr.cronExpression == "") {
                    this.cronExpression = this.defaultCronExpression;
                }
                else
                    this.cronExpression = "0 " + pr.cronExpression.substr(0, pr.cronExpression.length - 2) + " ? *";
            });
        });
    }

    updateProfile(event) {
        //Convert to the hanfire cron format
        if (this.isCronDisabled || (this.cronExpression == this.defaultCronExpression))
            this.profile.cronExpression = "";
        else
            this.profile.cronExpression = this.cronExpression.substr(2, this.cronExpression.length - 6) + " *";

        this.profileManagerService.updatePorfile(this.profile).subscribe(pr => {
            //wait until save process completes
            if (pr) {
                this.bsModalRef = this.modalService.show(MessageBoxComponent);
                this.bsModalRef.content.title = 'Success';
                this.bsModalRef.content.message = 'Profile:' + this.profile.profileName + ' successfully saved.';
            }
        }, (err) => {
            this.bsModalRef = this.modalService.show(ErrorMessageBoxComponent);
            this.bsModalRef.content.title = 'Error';
            this.bsModalRef.content.message = 'Error occured in saving profile: ' + this.profile.profileName;
        });
    }


}
