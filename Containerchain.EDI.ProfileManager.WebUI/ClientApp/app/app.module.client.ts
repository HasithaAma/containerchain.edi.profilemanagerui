import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { sharedConfig } from './app.module.shared';
import { MessageBoxComponent } from "./components/shared/ui/messagebox.component";
import { ErrorMessageBoxComponent } from "./components/shared/ui/errormessagebox.component";
import { NewOutgoingSettingDialogComponent } from "./components/filetransfer/outgoing/newoutgoingsettingdialog.component";
import { ModalBackdropComponent, ModalContainerComponent} from "ngx-bootstrap/modal";
import { NewOutputDialogComponent } from "./components/output/newoutputdialog.component";
import { NewRecipientDialogComponent } from "./components/filetransfer/outgoing/newrecipientdialog.component";
import { NewIncomingSettingDialogComponent } from './components/filetransfer/incoming/newincomingsettingdialog.component';
import { ConfirmBoxComponent } from './components/shared/ui/confirmbox.component';

@NgModule({
    bootstrap: sharedConfig.bootstrap,
    declarations: sharedConfig.declarations,
    imports: [
        BrowserModule,
        FormsModule,
        HttpModule,
        sharedConfig.imports
    ],
    //Don't forget to add the component to entryComponents section
    entryComponents: [
        MessageBoxComponent, ErrorMessageBoxComponent, ModalBackdropComponent, ModalContainerComponent, NewOutgoingSettingDialogComponent, NewOutputDialogComponent, NewRecipientDialogComponent, NewIncomingSettingDialogComponent, ConfirmBoxComponent
    ],
    providers: [
        { provide: 'ORIGIN_URL', useValue: location.origin }, sharedConfig.providers
    ]
})
export class AppModule {
}
