﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.IdentityModel.Clients.ActiveDirectory;

namespace Containerchain.EDI.ProfileManager.Models
{
    public class AppSettings
    {
        public ApiUrls ApiUrls { get; set; }

        public Authentication Authentication { get; set; }
    }

    public class ApiUrls
    {
        public string ProfileManagerApiBaseUrl { get; set; }

        public string MapperApiBaseUrl { get; set; }

        public string FileBankApiBaseUrl { get; set; }

        public string ReportApiBaseUrl { get; set; }
    }

    public class Authentication
    {

        public string Tenant { get; set; }

        public string ClientId { get; set; }

    }
}
